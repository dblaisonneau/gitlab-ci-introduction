---?image=https://gitlab.com/dblaisonneau/gitlab-ci-introduction/raw/master/assets/img/evolution-3801547_1920_dark.jpg

# GitLab-CI

+++
## Continuous integration

<blockquote>
L'intégration continue est un ensemble de pratiques utilisées en génie
logiciel consistant à vérifier à chaque modification de code source que le
résultat des modifications ne produit pas de régression dans l'application
développée. [...] Le principal
but de cette pratique est de détecter les problèmes d'intégration au plus tôt
lors du développement. De plus, elle permet d'automatiser l'exécution des
suites de tests et de voir l'évolution du développement du logiciel.</blockquote>
--wikipedia--

+++
## Continuous delivery

<blockquote>La livraison continue est une approche d’ingénierie logicielle dans laquelle
les équipes produisent des logiciels dans des cycles courts, ce qui permet de
le mettre à disposition à n’importe quel moment. Le but est de construire,
tester et diffuser un logiciel plus rapidement. L’approche aide à réduire le
coût, le temps et les risques associés à la livraison de changement en
adoptant une approche plus incrémentielle des modifications en production.
Un processus simple et répétable de déploiement est un élément clé.</blockquote>
--wikipedia--

+++

## L'écosystème

![ecosysteme CI](https://gitlab.com/dblaisonneau/gitlab-ci-introduction/raw/master/assets/img/ecosysteme.png)
Travis / Jenkins / TeamCity
+++

![GitLab CI/CD](https://docs.gitlab.com/ee/ci/img/cicd_pipeline_infograph.png)

+++

## Les Runners

Les *runners* sont les environnements d'exécution des tâches du CI/CD. Ils
peuvent êtres partagés, ou dédiés à un projet; sous différents environnements:
shell, docker, virtualbox, ssh, kubernetes.

+++

![Runners](https://gitlab.com/dblaisonneau/gitlab-ci-introduction/raw/master/assets/img/gitlab-ci-overview-small.png)


+++

## Pipelines/Stages/Jobs

Un pipeline est un ensemble de tâches (jobs) ordonnancées en plusieurs étapes
(stages)

+++

![Pipeline](https://gitlab.com/dblaisonneau/gitlab-ci-introduction/raw/master/assets/img/pipeline.png)

+++

## Les *Manifest*

Toute la description du CI/CD est centralisée dans un seul fichier YAML, le fichier ```.gitlab-ci.yaml```

```
job1:
  script: "execute-script-for-job1"

job2:
  script: "execute-script-for-job2"
```
+++

## Job console

Chaque job permet d'accéder en direct aux logs de l'execution de la tache.
![schedule](https://gitlab.com/dblaisonneau/gitlab-ci-introduction/raw/master/assets/img/console.png)

+++

## Job artifacts

Pour sauvegarder les résultats d'une tâche, il est possible de sauvegarder
une archive appelée artifact. Elle peut être disponible au téléchargement ou
simplement transmise d'une étape à l'autre.
![artifacts](https://gitlab.com/dblaisonneau/gitlab-ci-introduction/raw/master/assets/img/artifacts.png)

+++

## Pipelines variables

De nombreuses [variables](https://docs.gitlab.com/ee/ci/variables/) prédéfinies
sont disponibles pour chaque tâches, permettant de donner toutes les infos
nécessaires à propos:
* du projet
* du commit
* du merge request
* de l'utilisateur
* du runner
* du pipeline ...

+++

## Assigner une variable d'environnement

Des variables d'environnement peuvent aussi être assignées à chaque pipeline, de
manière publique ou privée.
![variables](https://gitlab.com/dblaisonneau/gitlab-ci-introduction/raw/master/assets/img/variables.png)

+++

## Déclanchement d'un Pipeline

* un push sur une branches GIT: push/merge-request/master/branches/tags
* un évènement API / trigger
* un autre pipeline
* un déclenchement programmé (cron)
* un formulaire web

+++

## Schedules

![schedule](https://gitlab.com/dblaisonneau/gitlab-ci-introduction/raw/master/assets/img/schedule.png)

+++

## Environnements

Un environnement permet de spécifier différents milieux de déploiement
différents: testing / production / site1 / site2.

![environnement](https://gitlab.com/dblaisonneau/gitlab-ci-introduction/raw/master/assets/img/environnement.png)

+++

## Kubernetes clusters

GitLab peut être connecté très simplement à un cluster *Kubernetes* existant, ou
même le créer tout seul sur *Google Cloud Platform*.
On peut ainsi y déployer
ses applications ou mettre à disposition un ensemble de runners.

![k8s_apps](https://docs.gitlab.com/ee/topics/autodevops/img/guide_cluster_apps.png)

+++

## Review Apps
![review_app](https://about.gitlab.com/images/review-apps/continuous-delivery-review-apps.svg)

+++

## AutoDevOPs

Il est possible de laisser GitLab générer tout seul toutes les étapes
du CI/CD, de manière <del>magique</del> automatique.
![autodevops](https://gitlab.com/help/topics/autodevops/img/guide_enable_autodevops.png)

+++

## AutoDevOPS - rollout

![Rollout](https://docs.gitlab.com/ee/topics/autodevops/img/rollout_enabled.png)

+++


## Serverless

Dans sa dernière release, GitLab permet avec l'intégration Kubernetes
l'utilisation de knative, une application de déploiement **serverless**.

Le serverless permet ainsi de créer de petits ensembles de code "scalable",
disponibles via une API Rest sous forme de F(unctions)aaS.

+++

![serverless](https://docs.gitlab.com/ee/user/project/clusters/serverless/img/serverless-page.png)
