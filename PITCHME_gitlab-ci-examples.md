---?image=https://gitlab.com/dblaisonneau/gitlab-ci-introduction/raw/master/assets/img/fan-3645379_1920_dark.jpg
# Quelques jobs

+++

## Simple scripts
```
job:
  before_script:
    - execute this instead of global before script
  script:
    - my command
  after_script:
    - execute this after my script
```

+++
## Utilisation des *stages*

```
stages:
  - build
  - test
  - deploy

job 1:
  stage: build
  script: make build dependencies

job 2:
  stage: build
  script: make build artifacts

job 3:
  stage: test
  script: make test

job 4:
  stage: deploy
  script: make deploy
```
@[1-4]
@[6-8]
@[10-12]
@[14-16]
@[18-20]

+++

### Linting: Ansible linting

```
ansible_linting:
  image: sdesbure/ansible-lint:latest
  script:
    - ansible-lint -x ANSIBLE0010,ANSIBLE0013 os_*.yml
  stage: lint
```
@[1]
@[2]
@[3-4]
@[5]

+++

### Linting: SSH Key check

```
sshkey_check:
  stage: lint
  script:
    - "keylist=$(cat users.yml|yq -r '.users[].sshkey')"
    - "for key in ${keylist}; do \
        ssh-keygen -l -f  files/keys/${key}.key.pub; \
      done"
```
@[1]
@[2]
@[3-7]

+++

## Deploy Python to production

```
test:
  script:
  # this configures Django application to use attached postgres database that is run on `postgres` host
  - export DATABASE_URL=postgres://postgres:@postgres:5432/python-test-app
  - apt-get update -qy
  - apt-get install -y python-dev python-pip
  - pip install -r requirements.txt
  - python manage.py test

staging:
  type: deploy
  script:
  - apt-get update -qy
  - apt-get install -y ruby-dev
  - gem install dpl
  - dpl --provider=heroku --app=gitlab-ci-python-test-staging --api-key=$HEROKU_STAGING_API_KEY
  only:
  - master

production:
  type: deploy
  script:
  - apt-get update -qy
  - apt-get install -y ruby-dev
  - gem install dpl
  - dpl --provider=heroku --app=gitlab-ci-python-test-prod --api-key=$HEROKU_PRODUCTION_API_KEY
  only:
  - tags
```
@[1-8]
@[10-18]
@[20-28]

+++

## Autobuild Docker Docker

![docker](https://gitlab.com/dblaisonneau/gitlab-ci-introduction/raw/master/assets/img/con_docker.png)

+++

### Auto builds

```
image: docker:git
services:
  - docker:dind
variables:
  DOCKER_DRIVER: overlay

before_script:
  - docker login -u gitlab-ci-token -p "$CI_BUILD_TOKEN" "$CI_REGISTRY"

development:
  stage: deploy
  script:
    - docker build -t "$CI_REGISTRY_IMAGE:latest" .
    - docker push "$CI_REGISTRY_IMAGE:latest"
  only:
    - master

testing:
  stage: deploy
  script:
    - docker build -t "$CI_REGISTRY_IMAGE:${CI_COMMIT_REF_SLUG}" .
    - docker push "$CI_REGISTRY_IMAGE:${CI_COMMIT_REF_SLUG}"
  only:
    - branches
  except:
    - master

stable:
  stage: deploy
  script:
    - docker build -t "$CI_REGISTRY_IMAGE:${CI_COMMIT_REF_SLUG}" .
    - docker push "$CI_REGISTRY_IMAGE:${CI_COMMIT_REF_SLUG}"
  only:
    - tags
```
@[1]
@[2-3]
@[4-5]
@[7-8]
@[10-16]
@[18-26]
@[28-34]
+++

### Docker Security scanning

```
container_scanning:
  image: docker:stable
  stage: test
  variables:
    DOCKER_DRIVER: overlay2
    ## Define two new variables based on GitLab's CI/CD predefined variables
    ## https://docs.gitlab.com/ee/ci/variables/#predefined-variables-environment-variables
    CI_APPLICATION_REPOSITORY: $CI_REGISTRY_IMAGE #/$CI_COMMIT_REF_SLUG
    CI_APPLICATION_TAG: latest #$CI_COMMIT_SHA
  allow_failure: true
  only:
    - master
  services:
    - docker:stable-dind
  script:
    - docker run -d --name db arminc/clair-db:latest
    - docker run -p 6060:6060 --link db:postgres -d --name clair --restart on-failure arminc/clair-local-scan:v2.0.1
    - apk add -U wget ca-certificates
    - docker pull ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG}
    - wget https://github.com/arminc/clair-scanner/releases/download/v8/clair-scanner_linux_amd64
    - mv clair-scanner_linux_amd64 clair-scanner
    - chmod +x clair-scanner
    - touch clair-whitelist.yml
    - while( ! wget -q -O /dev/null http://docker:6060/v1/namespaces ) ; do sleep 1 ; done
    - retries=0
    - echo "Waiting for clair daemon to start"
    - while( ! wget -T 10 -q -O /dev/null http://docker:6060/v1/namespaces ) ; do sleep 1 ; echo -n "." ; if [ $retries -eq 10 ] ; then echo " Timeout, aborting." ; exit 1 ; fi ; retries=$(($retries+1)) ; done
    - ./clair-scanner -c http://docker:6060 --ip $(hostname -i) -r gl-container-scanning-report.json -l clair.log -w clair-whitelist.yml ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG} || true
  artifacts:
    reports:
      container_scanning: gl-container-scanning-report.json
```

+++

## GitLab CI for GitLab Pages

Le système de *Pages* permet de mettre en place un site web statique hébergé
par GitLab. Ces pages *statiques* peuvent bien entendu être générées lors de
l'exécution de la tâche.

Les possibilité sont multiples:
* Blog sur base markdown
* Mise en page de résultats de tests
* Résultats d'un build précédent.
* ...

+++

![pages](https://about.gitlab.com/images/feature_page/screenshots/03-gitlab-pages.png)

+++

### Pages exemples

```
pages:
  stage: deploy
  image: jekyll:latest
  script:
    - cd doc
    - jekyll build -d ../public
  artifacts:
    paths:
      - public

```
