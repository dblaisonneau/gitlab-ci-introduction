---?image=https://gitlab.com/dblaisonneau/gitlab-ci-introduction/raw/master/assets/img/hand-3588162_1920_dark.jpg

# Un peu de pratique

  * Publier une image docker et la scanner
  * AutoDevOPs
  * Construire un package android
  * Le projet Gitlab-ce

+++

## Publier une image docker

Construire une image Docker, la publier et la tester

@fa[gitlab][https://gitlab.com/dblaisonneau/docker-node-red-smarthome/](https://gitlab.com/dblaisonneau/docker-node-red-smarthome/)

@fa[gitlab][https://www.objectif-libre.com/fr/blog/2018/07/26/scanning-docker-images-with-clair-and-gitlab/](https://www.objectif-libre.com/fr/blog/2018/07/26/scanning-docker-images-with-clair-and-gitlab/)

+++

## Serverless

@fa[gitlab][https://gitlab.com/dblaisonneau/knative-ruby-app/](https://gitlab.com/dblaisonneau/knative-ruby-app/)

+++

## AutoDevOPs

Créer un projet depuis un template, publier sur Kubernetes,
effectuer un merge request, tester ce changement et le passer
en prod.

@fa[gitlab][https://gitlab.com/help/topics/autodevops/quick_start_guide.md](https://gitlab.com/help/topics/autodevops/quick_start_guide.md)


  * app/views/welcome/index.html.erb
    ```
    <h1>Hi Code d'Armor !</h1>
    ```
  * test/controllers/welcome_controller_test.rb

+++

## Android

Construire une image Android

@fa[gitlab][https://about.gitlab.com/2018/10/24/setting-up-gitlab-ci-for-android-projects/](https://about.gitlab.com/2018/10/24/setting-up-gitlab-ci-for-android-projects/)

@fa[gitlab][https://gitlab.com/jlenny/androidblog-2018](https://gitlab.com/jlenny/androidblog-2018)

+++

## GitLab

Le projet Gitlab Community Edition

@fa[gitlab][https://gitlab.com/gitlab-org/gitlab-ce/pipelines](https://gitlab.com/gitlab-org/gitlab-ce/pipelines)
