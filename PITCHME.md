@snap[north]
# Gitlab-CI
@snapend

@snap[west]
![GitLab Icon](https://gitlab.com/dblaisonneau/gitlab-ci-introduction/raw/master/assets/img/GitLab-logo5_small.png)
@snapend

@snap[east]
Code d'Armor<br/>
15 janvier 2019<br/>
David Blaisonneau
@snapend


---?image=https://gitlab.com/dblaisonneau/gitlab-ci-introduction/raw/master/assets/img/microphone-1209816_1920_dark.jpg
# quelques mots
---?include=PITCHME_gitlab.md
---?include=PITCHME_gitlab-ci-presentation.md
---?include=PITCHME_gitlab-ci-examples.md
---?include=PITCHME_gitlab-ci-enpratique.md


---?image=https://gitlab.com/dblaisonneau/gitlab-ci-introduction/raw/master/assets/img/startup-3127285_1920_dark.jpg
# Quelques mots pour conclure

---?image=https://gitlab.com/dblaisonneau/gitlab-ci-introduction/raw/master/assets/img/lamp-3489395_1920_dark.jpg
# Merci
### vous devez avoir des questions, non ?

@fa[envelope-square]david-dev@blaisonneau.fr

@fa[twitter-square]dblaisonneau
