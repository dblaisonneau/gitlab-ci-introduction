---?image=https://gitlab.com/dblaisonneau/gitlab-ci-introduction/raw/master/assets/img/chess-3801531_1920_dark.jpg

# Gitlab <del>-CI</del>

+++

Gitlab est un logiciel de forge permettant de gérer toutes les étapes d'un cycle
de développement **DevOPS**. C'est:
  - à l'origine une simple forge au dessus de GIT
  - une application de gestion du Cycle **DevOPS**
  - plusieurs versions:
    - gitlab.com
    - en mode privé, pour son propre environnement

+++
## Le cycle devOPS

![Le cycle devops](https://gitlab.com/dblaisonneau/gitlab-ci-introduction/raw/master/assets/img/devops-loop-and-spans.png)

+++
## Les principales fonctions de Gitlab

On retrouve bien entendu:
  - la gestion de projets et des groupes de projets
  - la gestion des différents utilisateurs et des outils d'échanges:
    snippets, un wiki
  - la gestion et revue de code
  - un registry Docker
  - ...
et surtout de nombreuses fonctions autour du CI/CD !

+++
## La gestion de projets

Pour la gestion des projets:
  - différents niveaux d'utilisateurs (guest, reporter, developer,
    maintener, owner)
  - gestion des *issues* via un board de type KanBan
  - milestones (agile sprint / release)
  - time tracking, cycle analytics
  - service desk

+++

## La gestion du code

Tout est fait pour simplifier le gestion du code
  - WebIDE
  - versionning du code (via Git)
  - gestion simple des branches, des tags
  - *merge request* et revue de code
  - plein de beaux graphiques
  - le blocage de fichiers

+++

## Les *merge request*

Ils permettent entre autres:
  - de présenter l'ajout de code
  - de comparer le code résultant de plusieurs commits
  - commenter les choix des developpers
  - visualiser les changements en mode "Live preview"

et bien entendu on y retrouve les résultats du CI/CD !

+++

## Quelques exemples

* [Orange Kubespray #7](https://gitlab.com/Orange-OpenSource/lfn/infra/kubespray_automatic_installation/merge_requests/7)
* [Gitlab #24345](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/24345)
* [Gitlab #24181](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/24181)
